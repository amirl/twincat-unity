### 5.0.320
Enh: Updated Package Dependencies to 5.0 Versions

### 5.0.297
Enh: Updated Package Dependencies to 5.0 Versions

### 5.0.1-preview.12

### 5.0.1-preview.11
Enh: Target change dotnet core 3.0 --> 3.1

### 5.0.1-preview.10
Enh: Implementation of DynamicValue.UpdateMode to support immediate value update and cached value update scenarios.
Enh: Adding .NET Core 5.0 as platform

### 5.0.0-preview9
Enh: Null-Reference awareness in API Interfaces c# 8.0 (see [Nullable Reference Types ](https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/builtin-types/nullable-reference-types))

### 5.0.0-preview8

### 5.0.0-preview7

### 5.0.0-preview6
Enh: Extended Handling for AmsNetIds (SubNets), AmsNetId.IsSameTarget and AmsNetId.NetIdsEqual extended.

### 5.0.0-preview5
Enh: Serialization Support for Exception derived classes
Breaking Change: Refactoring TwinCAT.Ads.AdsException derived classes and organizing Namespaces.
Breaking Change: TwinCAT.Ads.ResultAds constructor now protected. Use the CreateError static methods instead.

### 5.0.0-preview4
Breaking Change: Changing Parameterset of AddDeviceNotification, TryAddDeviceNotification and AddDeviceNotificationAsync methods.
The Memory\<byte\> parameter is replaced by the dataLength argument to reduce array copy operations (enhanced Performance) and for simplyfied use.

### 5.0.0-preview3
Enh: Framework dependency changed net462 --> net462
Enh: Adding Framework target netcoreapp2.0
Enh: Optimizing Nuget Package dependencies

### 5.0.0-preview1
Fix: Gap year correction for PlcOpen DataTypes DT and DATE.

### 5.0.0-beta9
Enh: Referencing .NET Core 3.0 instead of .NET Core 2.2

### 5.0.0-beta7
Enh: SymbolicAnyTypeConverter added to marshal non-primitive-types with ReadSymbol/WriteSymbol
