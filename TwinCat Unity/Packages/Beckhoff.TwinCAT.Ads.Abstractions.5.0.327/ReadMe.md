﻿## Description
The package **'Beckhoff.TwinCAT.Ads.Abstractions'** contains interfaces and base implementations for the **'Beckhoff.TwinCAT.Ads.Server'** and
**'Beckhoff.TwinCAT.Ads'** packages. It is never used standalone and is a dependency of the above-named packages.

## Requirements
- **.NET 5.0**, **.NET Core 3.1**, **.NET Framework 4.61** or **.NET Standard 2.0** compatible SDK or later
- Latest **TwinCAT 3.1.4024** Build
- or alternatively for systems where a TwinCAT installation is not running the Nuget package **'Beckhoff.TwinCAT.Ads.AdsRouterConsole'**.
to route ADS communication.
- Installed Nuget package manager (for systems without Visual Studio installation)

## Installation
As dependency of other Beckhoff packages.

## Further documentation
The actual version of the documentation is available in the Beckhoff Infosys.
[Beckhoff Information System](https://infosys.beckhoff.com/index.php?content=../content/1033/tc3_ads.net/index.html&id=207622008965200265)

