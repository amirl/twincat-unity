﻿## Description
The package **'Beckhoff.TwinCAT.Ads.Server'** contains the base framework to create your own ADS Server / virtual ADS Device.

## Requirements
- **.NET 5.0**, **.NET Core 3.1**, **.NET Framework 4.61** or **.NET Standard 2.0** compatible SDK or later
- Latest **TwinCAT 3.1.4024** Build
- or alternatively for systems where a TwinCAT installation is not running the Nuget package **'Beckhoff.TwinCAT.Ads.AdsRouterConsole'**.
to route ADS communication.
- Installed Nuget package manager (for systems without Visual Studio installation)

## Installation

#### System with TwinCAT System Service (TCP Loopback enabled, Versions 4024.0 and 4025.5)
1. Running TwinCAT 3.1.4024 Installation
2. Stop TwinCAT System Service
3. Enable TCP Loopback for TwinCAT 3.1.4024.0 or 3.1.4024.4 via regkey:   
    32-Bit Windows Operating System:
    ```Registry
	HKEY_LOCAL_MACHINE\SOFTWARE\Beckhoff\TwinCAT3\System\EnableAmsTcpLoopbackDWORD (1)
    ```
    64-Bit Windows Operating System:
    ```Registry
	HKEY_LOCAL_MACHINE\SOFTWARE\WOW6432Node\Beckhoff\TwinCAT3\System\EnableAmsTcpLoopback	DWORD(1)
    ```

    Systems >= 3.1.4024.5 support TCPLoopback out of the box. No registry changes necessary.

4. Restart TwinCAT System Service 

## First Steps

Create your customized ADS Server by deriving the TwinCAT.Ads.Server.AdsServer class. Fill the virtual handlers with your own
code.

```csharp
using Microsoft.Extensions.Logging;
using System;
using System.Buffers.Binary;
using System.Collections.Concurrent;
using System.Threading;
using System.Threading.Tasks;
using TwinCAT.Ads;
using TwinCAT.Ads.Server;

namespace TestServer
{
    /*
     * Extend the AdsServer class to implement your own ADS server.
     */
    public class AdsSampleServer : AdsServer
    {
        /// <summary>
        /// Fixed ADS Port (to be changed ...)
        /// </summary>
        const ushort ADS_PORT = 42;

        /// <summary>
        /// Fixed Name for the ADS Port (change this ...)
        /// </summary>
        const string ADS_PORT_NAME = "AdsSampleServer_Port42";


        /// <summary>
        /// Logger
        /// </summary>
        private ILogger _logger;

        /* Instantiate an ADS server with a fix ADS port assigned by the ADS router.
        */


        public AdsSampleServer(ILogger logger) : base(ADS_PORT, ADS_PORT_NAME)
        {
            _logger = logger;
        }

        // Override Functions to implement customized Server
        ....
    }
}
```
## Further documentation
The actual version of the documentation is available in the Beckhoff Infosys.
[Beckhoff Information System](https://infosys.beckhoff.com/index.php?content=../content/1033/tc3_ads.net/index.html&id=207622008965200265)

