### 5.0.320
Enh: Updated Package Dependencies to 5.0 Versions

### 5.0.297

### 5.0.1-preview.12

### 5.0.1-preview.11
Enh: Target change dotnet core 3.0 --> 3.1

### 5.0.1-preview.10
Enh: Adding .NET Core 5.0 as platform
Enh: Implementing TwinCAT.Ads.Server.AdsServer.DeviceNotificationRequestAsync

### 5.0.0-preview9
Enh: Null-Reference awareness in API Interfaces c# 8.0 (see [Nullable Reference Types ](https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/builtin-types/nullable-reference-types))

### 5.0.0-preview8

### 5.0.0-preview7

### 5.0.0-preview6

### 5.0.0-preview5
Breaking Change: Removing the explicit .net core 2.0 platform support, .NET Standard 2.0 should be used instead.

### 5.0.0-preview4

### 5.0.0-preview3
Enh: Framework dependency changed net462 --> net462\
Enh: Adding Framework target netcoreapp2.0\
Enh: Optimizing Nuget Package dependencies\
Fix: TcpHeader wrong Frame size (Int16 overflow) with frame size > 65535 (large data packets)\
Breaking changes: AdsServer interfaces refactored, ReadLength parameters changed type uint --> int, Memory --> ReadOnlyMemory wherever possible.\

### 5.0.0-beta9
Enh: Referencing .NET Core 3.0 instead of .NET Core 2.2\
Fix: Fixing Buffer Issue in AdsServer.AddDeviceNotificationRequest\

### 5.0.0-beta8
Fix: Fixed port AdsServer forgets port with Disconnect() and ConnectServer sequence (uses unfixed port).\

### 5.0.0-beta7
BreakingChange: TwinCAT.Ads.Server.TcAdsNotificationSample renamed to TwinCAT.Ads.Server.NotificationDataSample\
BreakingChange: Refactoring AdsServer Interfaces. Optimizing Method parameters from byte[] + length to Memory<byte> to prevent multiple array copies (especially on NotificationHandling)\
Doc: Documentation for creating a custom ADS Server with .NET Core\

### 5.0.0-beta6
Enh: AmsServerNet.DefaultRouterEndPoint can be used to separate Router and AmsServer on different systems (Docker, Virtualization)\
