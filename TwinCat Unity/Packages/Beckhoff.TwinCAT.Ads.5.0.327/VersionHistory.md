### 5.0.327
Fix: Parallel usage of ADS notifications and Reactive extension leads to problems, fixes #34108

### 5.0.320
Fix: Browsing TC2 Globals with TwinCAT.TypeSystem.SymbolNavigator\<T\>
Fix: Recognition of PlcOpen DataTypes, probably fixing ADOS #32449
Fix: Exception on reading Array of StructMarshalled Custom Array #32997
Enh: Updated Nuget Package Dependencies to 5.0 Versions

### 5.0.297
### 5.0.1-preview.12
Fix: InstancePath on dereferenced Symbols (POINTER types, dereferencing order >= 2)
Fix: NullReferenceException on SumCreateHandles.CreateHandles() when an AdsError occurs (ADOS 25620)
Fix: RPC method calls are not possible with enum parameter (ADOS 25603)
Fix: Variable handles throw AdsErrorCode 0x703, DeviceIndexOffset Invalid when not created via CreateVariableHandle (ADOS 25859)
Fix: Resync SymbolEntry Parsing when the SymbolEntry cannot be read (ADOS 26910)

### 5.0.1-preview.11
Enh: Target change dotnet core 3.0 --> 3.1
Enh: Implementation of PCCHType and PVOIDType (specific DataTypes derived from PointerType).
Enh: RpcInvoke supporting PCCH in Parameters.
Fix: Supporting PlcOpen types in RPCInvoke (TOD,DT,TIME,DATE,LTIME,TIME_OF_DAY,DATE_AND_TIME)

### 5.0.1-preview.10
Fix: ArgumentOutOfRangeException on Reading Symbols with attributes (ADOS 20736)
Enh: Implementation of DynamicValue.UpdateMode to support immediate value update and cached value update scenarios.
Enh: Adding .NET Core 5.0 as platform

### 5.0.0-preview9
Enh: Null-Reference awareness in API Interfaces c# 8.0 (see [Nullable Reference Types ](https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/builtin-types/nullable-reference-types))

### 5.0.0-preview8
Fix: AdsClient.SymbolVersionChanged is not properly deregistered, leading to RouterMemory Leak after Disconnect.

### 5.0.0-preview6
Fix: ArgumentOutOfRange Exception on CNC GEO Task
Fix: TwinCAT.AdsSymbolLoaderSettings.Default corrected to 'Symbolic'
Enh: Refactoring InvokeRpcMethod (Support of out-parameters), Parameter checking enhanced
Enh: Support of platform specific data types 'UXINT, XINT, XWORD'.
Enh: Support of Recreating cached symbol handles on SymbolVersionChanged event when using AdsClient ReadSymbol, WriteSymbol methods and AddDeviceNotifications with SymbolPath.

### 5.0.0-preview5
Fix: Several minor fixes.
Fix: ArgumentOutOfRangeException with SByte types in PrimitiveTypeConverter.TryConvert
Enh: Support of Platform bound types UXINT, XINT, XWORD.
Enh: Refactoring RpcProperty getter/setter calls.
Enh: Reviewing external and internal interfaces
Enh: Extended UnitTests.
Breaking Change: Removing the explicit .net core 2.0 platform support, .NET Standard 2.0 should be used instead.

### 5.0.0-preview4
Breaking Change: Changing Parameterset of AddDeviceNotification, TryAddDeviceNotification and AddDeviceNotificationAsync methods.
The Memory\<byte\> parameter is replaced by the dataLength argument to reduce array copy operations (enhanced Performance) and for simplyfied use.

Fix: Corrections of text messages in 'Obsolete-warnings' where IndexGroup/IndexOffset is not used with the uint overload\
Fix: Gap year correction (year 2400) for PlcOpen DataTypes DT and DATE.\
Fix: Wrong implementation of the IsPersistant Datatype Flag (AdsDataTypeFlags.Persistent)\
Enh: Pointer support for InvokeRpcMethod in parameters.\
Fix: AdsNotifications are not received when size of data changes (e.g in Broadcast search)

### 5.0.0-preview3
Enh: Framework dependency for full framework changed (net462 --> net462)\
Enh: Adding Framework target netcoreapp2.0\
Enh: Optimizing Nuget Package dependencies\
Enh: Consequently using DateTimeOffset instead of DateTime in interfaces and for ADS data marshalling

### 5.0.0-preview2
BreakingChange: Renaming AdsClient/AdsSession/IAdsSymbolicAccess Members\
    | OldName             | NewName         |\
    | ------------------- | --------------- |\
    | ReadSymbol          | ReadValue       |\
    | TryReadSymbol       | TryReadValue    |\
    | ReadSymbolAsync     | ReadValueAsync  |\
    | WriteSymbol         | WriteValue      |\
    | TryWriteSymbol      | TryWriteValue   |\
    | WriteSymbolAsync    | WriteValueAsync |\
    | ReadSymbolInfo      | ReadSymbol      |\
    | TryReadSymbolInfo   | TryReadSymbol   |\
    | ReadSymbolInfoAsync | ReadSymbolAsync |

Fix: Asynchronity (Race-Conditions) with ADS Notifications.\
Enh: Refactoring internal synchronous code paths. Keeping Sync paths synchronous reduce mixing of sync/async code.\
Enh: Faster delivery of ADS Notifications

### 5.0.0-beta10
Enh: AdsSumNotification implementation. Performance optimized way to receive timestamped 'blocks' of Notifications.\
Enh: Refactored and optimized AdsNotifications (Refactored interfaces, Performance optimizations)
