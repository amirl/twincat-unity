﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimulationObject : MonoBehaviour, IRequireTwinCatHandler
{
    public ITwinCatHandler TwinCatHandler { get; set; }
    [SerializeField]
    public string pouName;
    public string varName;
}
