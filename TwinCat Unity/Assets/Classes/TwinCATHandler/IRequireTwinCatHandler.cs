﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TwinCAT.Ads;

public interface IRequireTwinCatHandler
{
    ITwinCatHandler TwinCatHandler { get; set; }
}

public interface ITwinCatHandler
{
    void InitializeConnection();
    bool ReadBool(string pou, string variableName);
    int ReadInt(string pou, string variableName);
    bool WriteInt(string pou, string variableName, int value);
    bool WriteBool(string pou, string variableName, bool value);
}
