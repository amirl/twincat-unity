﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Sensor : MonoBehaviour, IRequireTwinCatHandler
{
    public ITwinCatHandler TwinCatHandler { get; set; }
    // Start is called before the first frame update
    [SerializeField]
    public string sPouName;
    public string sState;
    private bool bState;
    private bool bLast;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        IR_Ray();
    }
    [SerializeField]
    private void IR_Ray()
    {
        var direction = this.transform.TransformDirection(Vector3.forward);
        Transform trans = this.transform;
        Transform child = trans.Find("Indicator");
        //leave the function if parent transform does not contain "Indicator" child
        if (child == null) return;
        var objectRendered = child.GetComponent<Renderer>();
        RaycastHit hit;
        Debug.DrawRay(this.transform.position, direction);
        if (Physics.Raycast(this.transform.position, direction, out hit, 0.9f))
        {
            objectRendered.material.color = new Color(0, 255, 0);
            if (!bState)
            {
                TwinCatHandler.WriteBool(sPouName, sState, true);
                bState = true;
            }
        }
        else
        {
            objectRendered.material.color = new Color(255, 0, 0);
            if (bState)
            {
                TwinCatHandler.WriteBool(sPouName, sState, false);
                bState = false;
            }
        }
    }

}

